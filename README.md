```
npm install
npm run dev
```

```
open http://localhost:3000
```

Perform above actions. Root page works, it loads `pages/index.html` successfully.

```
open http://localhost:3000/sub/
```

You should get `Internal Server Error` in the browser and a nasty error in the console like below:

```
[mf:inf] GET /sub/ 500 Internal Server Error (61ms)
Trace: ReferenceError: React is not defined
    at functionsWorker-0.8593977399043575.js:1508:33
    at Hono.dispatch (functionsWorker-0.8593977399043575.js:857:15)
    at Hono.fetch (functionsWorker-0.8593977399043575.js:696:19)
    at functionsWorker-0.8593977399043575.js:1493:15
    at next (functionsWorker-0.8593977399043575.js:1908:32)
    at Object.fetch (functionsWorker-0.8593977399043575.js:1922:20)
    at __facade_modules_fetch__ (functionsWorker-0.8593977399043575.js:1981:46)
    at __facade_invokeChain__2 (functionsWorker-0.8593977399043575.js:1532:10)
    at Object.next (functionsWorker-0.8593977399043575.js:1529:14)
    at jsonError (functionsWorker-0.8593977399043575.js:1946:32) {
  stack: ReferenceError: React is not defined
    at functi…r (functionsWorker-0.8593977399043575.js:1946:32),
  message: React is not defined
}
    at logConsoleMessage (/home/csirolli/cloudflare/cfp-hono-jsx/node_modules/wrangler/wrangler-dist/cli.js:127502:25)
    at WebSocket2.<anonymous> (/home/csirolli/cloudflare/cfp-hono-jsx/node_modules/wrangler/wrangler-dist/cli.js:127221:13)
    at callListener (/home/csirolli/cloudflare/cfp-hono-jsx/node_modules/wrangler/wrangler-dist/cli.js:108142:18)
    at WebSocket2.onMessage (/home/csirolli/cloudflare/cfp-hono-jsx/node_modules/wrangler/wrangler-dist/cli.js:108084:13)
    at WebSocket2.emit (node:events:525:35)
    at Receiver2.receiverOnMessage (/home/csirolli/cloudflare/cfp-hono-jsx/node_modules/wrangler/wrangler-dist/cli.js:109018:24)
    at Receiver2.emit (node:events:513:28)
    at Receiver2.dataMessage (/home/csirolli/cloudflare/cfp-hono-jsx/node_modules/wrangler/wrangler-dist/cli.js:107628:18)
    at Receiver2.getData (/home/csirolli/cloudflare/cfp-hono-jsx/node_modules/wrangler/wrangler-dist/cli.js:107570:21)
    at Receiver2.startLoop (/home/csirolli/cloudflare/cfp-hono-jsx/node_modules/wrangler/wrangler-dist/cli.js:107347:26)
```