import { Hono } from 'hono';
import type { Context } from 'hono';
import { handle } from 'hono/cloudflare-pages';

const siteTitle = 'Test';

const Layout = (props: { children?: string, title?: string }) => {
  return (<html>
    <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
      <title>{props.title} - {siteTitle}</title>
    </head>
    <body>
      {props.children}
    </body>
  </html>)
}

const app = new Hono().basePath('/sub/')

app.get('/', (c) => {
  return c.html(<Layout title="Homepage">
    <div>
      <h1>Home</h1>
      <p>This is a JSX test.</p>
    </div>
  </Layout>);
});

app.get('/hello', async (c: Context) => {
  let helloPage = await import('../../test.tsx');
  return c.html(<>{helloPage}</>);
})

export const onRequest = handle(app)
